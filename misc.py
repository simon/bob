# Miscellaneous support functions for my build system.

import string
import os
import shutil
import subprocess
import threading

import log

shelllist = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
shelllist = shelllist + "%+,-./:=[]^_@"

class builderr(Exception):
    def __init__(self, s):
        self.s = s
    def __str__(self):
        return self.s

def checkstr(s, allowed_chars):
    # Return true iff the string s is composed entirely of
    # characters in the string `allowed_chars'.
    for c in s:
        if not (c in allowed_chars):
            return 0
    return 1

def shellquote(list):
    # Take a list of words, and produce a single string which
    # expands to that string of words under POSIX shell quoting
    # rules.
    ret = ""
    sep = ""
    for word in list:
        if not checkstr(word, shelllist):
            word = "'" + word.replace("'", "'\\''") + "'"
        ret = ret + sep + word
        sep = " "
    return ret

def numeric(s):
    return checkstr(s, "0123456789")

def rm_rf(dir):
    shutil.rmtree(dir, 1)

class popen_base:
    # Allow popen_[rw] to behave as context managers, and
    # automatically reap their subprocess once they're finished.
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        exitstatus = self.close()
        if exc_type is None and exitstatus != 0:
            raise misc.builderr("command terminated with status {:d}: {}"
                                .format(exitstatus, self.command))
        return False

class popen_r(popen_base):
    # Mimic os.popen(shell-command, "r").
    def __init__(self, command):
        self.command = command
        self.p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    def read(self, *args, **kws):
        return self.p.stdout.read(*args, **kws).decode()
    def readline(self, *args, **kws):
        return self.p.stdout.readline(*args, **kws).decode()
    def close(self):
        self.p.stdout.close()
        return self.p.wait()

class popen_w(popen_base):
    # Mimic os.popen(shell-command, "w").
    def __init__(self, command):
        self.command = command
        self.p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE)
    def write(self, data):
        self.p.stdin.write(data.encode())
    def close(self):
        self.p.stdin.close()
        return self.p.wait()

class popen_rb(popen_base):
    # Like popen_r, but expects bytes rather than str.
    def __init__(self, command):
        self.command = command
        self.p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        self.read = self.p.stdout.read
        self.readline = self.p.stdout.readline
    def close(self):
        self.p.stdout.close()
        return self.p.wait()

class popen_wb(popen_base):
    # Like popen_w, but expects bytes rather than str.
    def __init__(self, command):
        self.command = command
        self.p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE)
        self.write = self.p.stdin.write
    def close(self):
        self.p.stdin.close()
        return self.p.wait()

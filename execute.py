# Lexer and variable-expander for the scripts used in my build system.

import sys
import string
import os
import time
import threading
import select
import socket
import subprocess

import misc
import log
import checkout
import name

onecharvars = {}
multicharvars = {}

builddate = None

Err = object() # stored in multicharvars to indicate inaccessibility
class VarInvalid(BaseException): pass

def save_vars():
    return (onecharvars.copy(), multicharvars.copy())

def restore_vars(t):
    onecharvars, multicharvars = t

def set_onecharvar(var, val):
    onecharvars[var] = val

def set_multicharvar(var, val):
    multicharvars[var] = val

def set_multicharvar_to_err(var):
    multicharvars[var] = Err

def get_onecharvar(var):
    if var not in onecharvars:
        raise misc.builderr("variable `$%c' not found" % var)
    return onecharvars[var]

def get_multicharvar(var, default=None):
    ret = multicharvars.get(var, default)
    if ret is Err:
        raise VarInvalid()
    if isinstance(ret, tuple):
        # Function and parameter.
        ret = ret[0](ret[1])
    return ret

def unset_multicharvar(var):
    if multicharvars.has_key(var):
        del multicharvars[var]

def expand_varfunc(var, cfg):
    global builddate

    # Expand a variable or function enclosed in $(...). Takes a
    # string containing the text from inside the parentheses (after
    # any further expansion has been done on that); returns a
    # string containing the expansion.

    if var[0] == "!":
        # `$(!' introduces a special function.
        try:
            pos = var.index(" ")
            fn, val = var[1:pos], var[pos+1:]
        except ValueError:
            fn, val = var[1:], ""

        if fn == "numeric":
            # The entire function call has already been lexed, so
            # don't lex it again.
            log.logmsg("testing numericity of `%s'" % val)
            if misc.numeric(val):
                return "yes"
            else:
                return "no"
        elif fn == "available":
            log.logmsg("testing availability of variable `%s'" % val)
            try:
                get_multicharvar(val)
                return "yes"
            except VarInvalid:
                return "no"
        elif fn == "builddate":
            if val != "":
                raise misc.builderr("$(!builddate) expects no arguments")
            if builddate is None:
                # Invent a build date which will be used unless we
                # have a cache that overrides it.
                builddate = time.strftime("%Y%m%d",time.localtime(time.time()))
                if cfg.builddatecache is None:
                    log.logmsg("using uncached build date " + builddate)
                else:
                    verdata = checkout.verdata()
                    log.logmsg("this builddate verdata: " +
                               verdata.rstrip("\r\n"))

                    cachefile = os.path.join(cfg.builddatecache, cfg.mainmodule)
                    log.logmsg("looking for build date cache file " +
                               cachefile)

                    new = True
                    if os.path.exists(cachefile):
                        with open(cachefile, "r") as f:
                            last_builddate = f.readline().rstrip("\r\n")
                            last_verdata = f.read()
                        log.logmsg("last builddate verdata: " +
                                   last_verdata.rstrip("\r\n"))
                        if verdata == last_verdata:
                            log.logmsg("verdata matches")
                            new = False
                        else:
                            log.logmsg("verdata does not match")
                    else:
                        log.logmsg("cache file does not exist")

                    if new:
                        try:
                            os.mkdir(cfg.builddatecache, 0o700)
                        except FileExistsError:
                            pass
                        with open(cachefile + ".tmp", "w") as f:
                            f.write(builddate + "\n" + verdata)
                        os.rename(cachefile + ".tmp", cachefile)
                        log.logmsg("caching new build date " + builddate)
                    else:
                        builddate = last_builddate
                        log.logmsg("reusing last build date " + builddate)
            return builddate
        else:
            raise misc.builderr("unexpected string function `%s'" % var)
    else:
        # Just look up var in our list of variables, and return its
        # value.
        try:
            return get_multicharvar(var, "")
        except VarInvalid:
            raise misc.builderr("special variable `%s' is not currently valid" % var)

delegate_subproc = None

def delegate_stderr_threadfn():
    global delegate_stderr
    delegate_stderr = delegate_stderr_file.read()

def delegate_open(hosttype, cfg):
    global delegate_subproc
    global delegate_stdin_file
    global delegate_stdout_file
    global delegate_stderr_file
    global delegate_stderr
    global delegate_stderr_thread

    # Re-run the config file to find out what actual host to
    # connect to for the given host type.
    save = save_vars()
    run(cfg.cfgscript)
    host = get_multicharvar("host_" + hosttype)
    sshid = get_multicharvar("id_" + hosttype)
    usercmd = get_multicharvar("cmd_" + hosttype)
    docker = get_multicharvar("docker_" + hosttype)
    restore_vars(save)

    # Open a connection to the delegate host.
    if usercmd != None:
        delcmd = usercmd
    elif docker != None:
        bob_path = os.path.dirname(os.path.abspath(__file__))
        socket_dir = os.path.join(cfg.workpath, ".podman-socket")
        os.makedirs(socket_dir)
        socket_path = os.path.join(socket_dir, "socket")
        socket_listening = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        socket_listening.bind(socket_path)
        socket_listening.listen(1)
        delcmd = ["podman", "run", "-i", "--rm",
                  "-h", "bob-container",
                  "-v", bob_path + ":/bob:ro",
                  "-v", socket_dir + ":/socket:ro",
                  docker]
        setarch = get_multicharvar("docker_setarch_" + hosttype)
        if setarch != None:
            delcmd += ["setarch", setarch, "--"]
        delcmd += ["/bob/bob-delegate-server", "--socket=/socket/socket"]
        delcmd = misc.shellquote(delcmd)
    else:
        if hosttype == "-":
            # Special case: a host name of "-" causes a
            # self-delegation, i.e. we invoke the delegate
            # server directly rather than bothering with ssh.
            for pdir in sys.path:
                delcmd = pdir + "/" + name.server
                if os.path.exists(delcmd):
                    break
                delcmd = None
            if delcmd == None:
                raise misc.builderr("unable to find delegate server")
            delcmd = [delcmd]
        else:
            if host == "" or host is None:
                raise misc.builderr("configuration does not specify a host for"
                                    " delegate type `%s'" % hosttype)
            delcmd = ["ssh"]
            # If the user has specified an SSH identity key, use it.
            if sshid != None:
                delcmd = ["SSH_AUTH_SOCK="] + delcmd + ["-i", sshid]
            delcmd.append(host)
            delcmd.append(name.server)
        delcmd = misc.shellquote(delcmd)
    log.logmsg("Starting delegation to host type `%s'" % hosttype)
    log.logmsg("  Running delegation command: " + delcmd)

    kws = {
        "shell": True,
        "stderr": subprocess.PIPE,
        "close_fds": True,
    }

    if docker is None:
        kws["stdin"] = kws["stdout"] = subprocess.PIPE
    else:
        kws["stdin"] = open(os.devnull, "rb")
        kws["stdout"] = open(os.devnull, "wb")

    delegate_subproc = subprocess.Popen(delcmd, **kws)
    # Start a thread that wait()s for the subprocess, and closes the
    # write end of a pipe when it terminates.
    wait_pipe_r, wait_pipe_w = os.pipe()
    def wait_threadfn():
        try:
            delegate_subproc.wait()
        finally:
            os.close(wait_pipe_w)
    wait_thread = threading.Thread(target=wait_threadfn)
    wait_thread.setDaemon(True) # don't keep whole process alive
    wait_thread.start()

    # Python is bizarrely deficient in functions to read from a
    # pipe but only get as much data as is currently available, so
    # we handle stderr from the delegate by spawning a subthread :-(
    delegate_stderr_file = delegate_subproc.stderr
    delegate_stderr = ""
    delegate_stderr_thread = threading.Thread(
        target=delegate_stderr_threadfn)
    delegate_stderr_thread.setDaemon(True) # don't keep whole process alive
    delegate_stderr_thread.start()

    if docker is None:
        delegate_stdin_file = delegate_subproc.stdin
        delegate_stdout_file = delegate_subproc.stdout
    else:
        # Wait for either a connection to our socket, or EOF on the
        # delegate's stderr (indicating that it exploded before
        # getting that far), whichever comes first.
        rout, _, _ = select.select(
            [socket_listening.fileno(), wait_pipe_r], [], [])
        if socket_listening.fileno() in rout:
            socket_connected, _ = socket_listening.accept()
            delegate_stdin_file = socket_connected.makefile('wb')
            delegate_stdout_file = socket_connected.makefile('rb')
        else:
            log.logmsg("  Delegate server terminated at startup")
            log_delegate_stderr()
            raise misc.builderr("failed to start delegate server")
        socket_listening.close()

   # Wait for the announcement from the far end which says the
    # delegate server is running.
    while 1:
        s = delegate_stdout_file.readline()
        if s == b"":
            log.logmsg("  Unexpected EOF from delegate server at startup")
            log_delegate_stderr()
            raise misc.builderr("unexpected EOF from delegate server")
        s = s.rstrip(b"\r\n")
        if s == name.server_banner:
            log.logmsg("  Successfully started delegate server")
            break

    if docker is not None:
        os.unlink(socket_path)
        os.rmdir(socket_dir)

def delegate_write(data):
    delegate_stdin_file.write(data)
    delegate_stdin_file.flush()

def delegate_read(length):
    toret = delegate_stdout_file.read(length)
    if len(toret) < length:
        raise misc.builderr("unexpected EOF from delegate server")
    return toret

def log_delegate_stderr():
    global delegate_stderr_thread
    if delegate_stderr_thread is None:
        return
    delegate_stderr_thread.join()
    delegate_stderr_thread = None
    if len(delegate_stderr) > 0:
        log.logmsg("Standard error from delegation command:")
        for line in delegate_stderr.splitlines():
            log.logoutput(line)

def delegate_close():
    global delegate_subproc

    # Close the delegate session
    delegate_stdin_file.write(b"Q")
    delegate_stdin_file.close()
    delegate_stdout_file.close()
    delegate_subproc = None
    log_delegate_stderr()
    log.logmsg("Closed delegate session")

def run(cmdfuncs):
    for func in cmdfuncs:
        func()

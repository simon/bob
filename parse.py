# Lexer and parser for the scripts used in my build system. Also
# contains a lot of the code to actually run the script commands,
# which is wrapped up into arrays of Python functions.

import sys
import os
import glob
import struct
import shutil
import string
import time

import log
import checkout
import misc
import execute

whitespace = " \t"

removenewlines = str.maketrans("\r\n", "  ")

def constfn(s):
    # Return a function that returns s.
    return lambda: s

def concatenator(fns):
    # Utility function. Given a list of nullary functions each
    # returning a string, return a nullary function which returns
    # their concatenation.
    return lambda: "".join(f() for f in fns)

def internal_lex(s, terminatechars, permit_comments, permit_variables, cfg):
    # Lex string s until a character in `terminatechars', or
    # end-of-string, is encountered. Return a tuple consisting of
    #  - a lambda that will return the lexed and expanded text before
    #    the terminating character
    #  - the remainder of the string, including the terminating
    #    character if any

    outfns = []
    n = 0
    while n < len(s):
        c = s[n]
        if permit_comments and c == "#":
            break # effectively end-of-string
        if c in terminatechars:
            return (concatenator(outfns), s[n:])

        n = n + 1
        if c == "$":
            if not permit_variables:
                raise misc.builderr("variable expansions not permitted in "
                                    "this word")
            # Things beginning with a dollar sign are special.
            c2 = s[n]
            n = n + 1
            if c2 == "$" or c2 == "#":
                outfns.append(constfn(c2))
            elif c2 == "(":
                # We have a $(...) construct. Recurse to parse the
                # stuff inside the parentheses.
                varnamefn, s2 = internal_lex(s[n:], ")", False,
                                             permit_variables, cfg)
                if len(s2) == 0:
                    raise misc.builderr("`$(' without terminating `)'")
                outfns.append(lambda f=varnamefn:
                              execute.expand_varfunc(f(), cfg))
                s = s2
                n = 1  # eat the terminating paren
            else:
                outfns.append(lambda c=c2: execute.get_onecharvar(c))
        else:
            outfns.append(constfn(c))

    # If we reach here, this is the end of the string.
    return (concatenator(outfns), "")

def get_wordfn(s, cfg, permit_variables=True):
    # Extract one word from the start of a given string.
    # Returns a tuple containing
    #  - None if there was no word, or else a lambda which can be run
    #    later to return the expanded word. (Variable expansion is
    #    deferred until the lambda is run.)
    #  - the rest of the string

    # Skip initial whitespace.
    while len(s) > 0 and s[0] in whitespace:
        s = s[1:]

    # If there's nothing left in the string at all, return no word.
    if len(s) == 0:
        return (None, "")

    # If there's a double quote, lex until the closing quote, and
    # treat doubled quotes in mid-string as literal quotes. We
    # expect whitespace or EOS immediately after the closing quote.
    if s[0] == '"':
        wordfns = []
        sepfn = constfn("")
        while s[:1] == '"':
            fragfn, s = internal_lex(s[1:], '"', False, permit_variables, cfg)
            wordfns.extend([sepfn, fragfn])
            if s[:1] != '"':
                raise misc.builderr("unterminated double quote")
            s = s[1:]
            sepfn = constfn('"')
        if len(s) > 0 and not (s[0] in whitespace):
            raise misc.builderr("expected whitespace after closing double quote")
        return (concatenator(wordfns), s)

    # Otherwise, just scan until whitespace.
    return internal_lex(s, whitespace, True, permit_variables, cfg)

def get_keyword(s, cfg):
    # Extract one word from the start of the string, which may not
    # contain any variable expansions. Returns a tuple like
    # get_wordfn, except that the lambda has been evaluated already.
    wordfn, tail = get_wordfn(s, cfg, permit_variables=False)
    word = None if wordfn is None else wordfn()
    return word, tail

def lex_all(s, cfg):
    # Lex the entirety of a string. Returns a lambda which will give
    # the lexed version.
    linefn, empty = internal_lex(s, "", True, True, cfg)
    assert empty == ""
    return linefn

def trim(s):
    # Just trim whitespace from the front of a string.
    while len(s) > 0 and s[0] in whitespace:
        s = s[1:]
    return s

class ParseState:
    def __init__(self, is_config):
        self.seen_module = False
        self.delegated = False
        self.is_config = is_config
        self.program = []
        self.current = self.program
        self.stack = [("top", self.current)]
    def parseerr(self, msg):
        prefix = "%s:%d" % (self.scriptfile, self.lineno)
        if self.cmdno > 1:
            prefix += " #%d" % self.cmdno
        raise misc.builderr(prefix + ": parse error: " + msg)

def parse_script_line(ps, s, cfg, keyword=None):
    # Parse the script line given in s. Return a list of functions to
    # be called in turn by execute.run().

    # Trim the newline off the end of the string, to begin with.
    while s[-1:] == "\r" or s[-1:] == "\n":
        s = s[:-1]

    if keyword is not None:
        # Previous parsing work has already lexed the initial keyword.
        # Put it back on to s, so that logging s below will log the
        # full command.
        if s[:1] not in whitespace:
            s = " " + s
        s = keyword + s

    w, sr = get_keyword(s, cfg)

    if w == None or w == "":
        return # no command on this line

    # Early processing of elif and else, so that we log the elif
    # statement being tested in spite of ps.current _right now_
    # pointing at the branch we weren't executing, and likewise so
    # that we log the else statement (just for neatness).
    if w.startswith("elif"):
        context = ps.stack.pop()
        if context[0] != "if":
            ps.parseerr("`%s' without a prior matching if statement" % w)
        ps.current = context[2] # now we parse into that if's else clause
        w = w[2:] # and treat this as its corresponding if
    elif w == "else":
        context = ps.stack.pop()
        if context[0] != "if":
            ps.parseerr("`%s' without a prior matching if statement" % w)
        ps.current = context[2]
        ps.stack.append(("else", ps.current))

    # Log every line executed by a non-config script.
    if not ps.is_config:
        fn = os.path.basename(ps.scriptfile)
        line = ps.lineno
        cmd = ps.cmdno
        def logfn():
            log.logscript(fn, line, cmd, s)
        ps.current.append(logfn)

    if w.startswith("if") or w.startswith("elif"):
        cond = w[2:]
        if cond.startswith("n"):
            cond = cond[1:]
            invert = True
        else:
            invert = False
        if cond == "eq":
            w1fn, sr = get_wordfn(sr, cfg)
            w2fn, sr = get_wordfn(sr, cfg)
            def test():
                w1 = w1fn()
                w2 = w2fn()
                log.logmsg("testing string equality of `%s' and `%s'" % (
                    w1, w2))
                return w1 == w2
        elif cond == "exist":
            filenamefn, sr = get_wordfn(sr, cfg)
            def test():
                filename = filenamefn()
                log.logmsg("testing existence of `%s'" % filename)
                return os.path.exists(os.path.join(cfg.workpath, filename))
        else:
            ps.parseerr("unrecognised if statement `%s'" % w)

        thenclause = []
        elseclause = []

        def run():
            success = test()
            if invert:
                success = not success
            if success:
                execute.run(thenclause)
            else:
                execute.run(elseclause)
        ps.current.append(run)

        nextkw, sr = get_keyword(sr, cfg)
        if nextkw == "then":
            # Multi-line if. Push a context to spot elses, elifs and endifs.
            ps.stack.append(("if", thenclause, elseclause))
            ps.current = thenclause
        else:
            # Single-line if. Recurse to parse the rest of this line
            # into thenclause[], and leave elseclause empty.
            ps.stack.append(("singlelineif", thenclause))
            ps.current = thenclause
            parse_script_line(ps, sr, cfg, nextkw)
            context = ps.stack.pop()
            ps.current = ps.stack[-1][1]
            if context[0] != "singlelineif":
                ps.parseerr("`%s' control flow command in single-line if" % (
                    nextkw))
    elif w == "else":
        pass # we already processed this further up
    elif w == "endif":
        context = ps.stack.pop()
        if context[0] != "if" and context[0] != "else":
            ps.parseerr("`%s' without a prior matching if statement" % w)
        ps.current = ps.stack[-1][1]
    elif w == "set":
        # Set a variable.
        varfn, sr = get_wordfn(sr, cfg)
        valfn = lex_all(trim(sr), cfg)
        def run(verbose = not ps.is_config):
            var = varfn()
            val = valfn()
            if verbose:
                log.logmsg("Setting variable `%s' to value `%s'" % (var,val))
            execute.set_multicharvar(var, val)
        ps.current.append(run)
    elif w == "read":
        # Set a variable by reading from a file.
        varfn, sr = get_wordfn(sr, cfg)
        filenamefn, sr = get_wordfn(sr, cfg)
        def run(verbose = not ps.is_config):
            var = varfn()
            filename = os.path.join(cfg.workpath, filenamefn())
            if verbose:
                log.logmsg("Reading file `%s'" % (filename))
            with open(filename, "r") as f:
                val = f.read()
            val = val.rstrip("\r\n")
            if verbose:
                log.logmsg("Setting variable `%s' to value `%s'" % (var,val))
            execute.set_multicharvar(var, val)
        ps.current.append(run)
    elif w == "mkdir":
        dirfn, sr = get_wordfn(sr, cfg)
        modefn, sr = get_wordfn(sr, cfg)
        def run(verbose = not ps.is_config):
            dirpath = dirfn()
            mode = int(modefn(),8) if modefn is not None else 0o777
            if ps.is_config:
                if not dirpath.startswith("/"):
                    raise misc.builderr(
                        "mkdir of non-absolute path '%s' in config file" %
                        dirpath)
            else:
                if dirpath.startswith("/"):
                    raise misc.builderr(
                        "mkdir of absolute path '%s' not in config file" %
                        dirpath)
                dirpath = os.path.join(cfg.workpath, dirpath)
            if verbose:
                log.logmsg("Creating directory path `%s'" % (dirpath))
            try:
                os.makedirs(dirpath, mode)
            except FileExistsError:
                pass
        ps.current.append(run)
    elif w == "in" or w == "in-dest":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        if ps.delegated and w != "in":
            ps.parseerr("`in-dest' command invalid during delegation" % w)
        dirfn, sr = get_wordfn(sr, cfg)
        features = []
        while True:
            preposition, sr = get_keyword(sr, cfg)
            if preposition.split("/")[:1] == ["do"]:
                do = preposition
                break
            elif preposition == "with":
                featurefn, sr = get_wordfn(sr, cfg)
                features.append(featurefn)
            else:
                ps.parseerr("expected `do' or `with', saw `%s'" %
                                    preposition)
        do_with_opts = do.split("/")
        do = do_with_opts[0]
        delegate_cmd = b"C"
        for do_opt in do_with_opts[1:]:
            if do_opt == "win":
                if not ps.delegated:
                    ps.parseerr(
                        "unexpected `do/win' outside a delegate session" % w)
                delegate_cmd = b"W"
        if do != "do":
            ps.parseerr("expected `do' after `%s'" % w)
        cmdfn = lex_all(trim(sr), cfg)

        def add_features(cmd):
            if len(features) > 0:
                if delegate_cmd == b"W":
                    featurecmds = []
                    for featurefn in features:
                        feature = featurefn()
                        featurecmd = execute.get_multicharvar(
                            "winfeature_" + feature)
                        if featurecmd is None:
                            raise misc.builderr(
                                "unrecognised feature name `%s'" % feature)
                        featurecmds.append(featurecmd)
                    cmd = " & ".join(featurecmds + [cmd])
                else:
                    featurecmds = []
                    for featurefn in features:
                        feature = featurefn()
                        featurecmd = execute.get_multicharvar(
                            "feature_" + feature)
                        if featurecmd is None:
                            raise misc.builderr(
                                "unrecognised feature name `%s'" % feature)
                        featurecmds.append(featurecmd)
                    cmd = " && ".join(featurecmds + [cmd])
            return cmd

        if ps.delegated:
            def run():
                dir = dirfn()
                cmd = add_features(cmdfn())
                log.logmsg("Running command on delegate server: " + cmd)
                # Instead of running the command locally, send it to
                # the delegate host, and receive in return some output
                # and an exit code.
                dir_e = dir.encode()
                cmd_e = cmd.encode()
                execute.delegate_write(
                    delegate_cmd +
                    struct.pack(">L", len(dir_e)) + dir_e +
                    struct.pack(">L", len(cmd_e)) + cmd_e)

                # Retrieve the build command's output, line by line.
                output = b""
                while 1:
                    outlen = struct.unpack(">L", execute.delegate_read(4))[0]
                    if outlen == 0:
                        break
                    output += execute.delegate_read(outlen)
                    while 1:
                        newline = output.find(b"\n")
                        if newline < 0:
                            break
                        line = output[:newline]
                        output = output[newline+1:]
                        while line[-1:] == b"\r" or line[-1:] == b"\n":
                            line = line[:-1]
                        log.logoutput(line)

                # Log the final partial line, if any.
                if len(output) > 0:
                    while output[-1:] == "\r" or output[-1:] == "\n":
                        output = output[:-1]
                    log.logoutput(output)

                exitcode = struct.unpack(">l", execute.delegate_read(4))[0]
                if exitcode > 0:
                    raise misc.builderr(
                        "build command terminated with status %d" % exitcode)
        else:
            def run():
                if w == "in-dest":
                    dir = os.path.join(cfg.outpath, dirfn())
                else:
                    dir = os.path.join(cfg.workpath, dirfn())
                cmd = add_features(cmdfn())
                log.logmsg("Running command in directory `%s': %s" % (dir,cmd))
                cmd = (misc.shellquote(["cd", dir]) +
                       " && { " + cmd + "; } 2>&1")
                f = misc.popen_r(cmd)
                while 1:
                    line = f.readline()
                    if line == "": break
                    while line[-1:] == "\r" or line[-1:] == "\n":
                        line = line[:-1]
                    log.logoutput(line)
                ret = f.close()
                if ret > 0:
                    raise misc.builderr(
                        "build command terminated with status %d" % ret)
        ps.current.append(run)
    elif w == "deliver":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        srcpathfn, sr = get_wordfn(sr, cfg)
        sr = trim(sr)
        dstfilefn, sx = get_wordfn(sr, cfg)
        def run():
            nfiles = 0
            for srcfile in glob.glob(os.path.join(cfg.workpath, srcpathfn())):
                if os.path.isdir(srcfile):
                    continue
                save = execute.save_vars()
                execute.set_onecharvar("@", os.path.basename(srcfile))
                dstfile = os.path.join(cfg.outpath, dstfilefn())
                execute.restore_vars(save)
                log.logmsg("Delivering `%s' to `%s'" % (srcfile, dstfile))
                dstdir = os.path.dirname(dstfile)
                if not os.path.exists(dstdir):
                    os.makedirs(dstdir)
                shutil.copyfile(srcfile, dstfile)
                nfiles += 1
            if nfiles == 0:
                raise misc.builderr("deliver statement did not match any files")
        ps.current.append(run)
    elif w == "checkout":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        modulefn, sr = get_wordfn(sr, cfg)
        destdirfn, sr = get_wordfn(sr, cfg)
        if modulefn == None or destdirfn == None:
            ps.parseerr("`checkout' command expects two parameters")
        def run():
            module = modulefn()
            destdir = os.path.join(cfg.workpath, destdirfn())
            checkout.checkout(cfg, module, destdir, 0)
        ps.current.append(run)
    elif w == "module":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        newmodulefn, sr = get_wordfn(sr, cfg)
        if newmodulefn == None:
            ps.parseerr("`module' command expects a parameter")
        def run():
            newmodule = newmodulefn()
            srcdir = os.path.join(cfg.workpath, cfg.mainmodule)
            destdir = os.path.join(cfg.workpath, newmodule)
            if srcdir == destdir:
                log.logmsg("main module already has correct filename")
            else:
                log.logmsg("renaming main module directory `%s' to `%s'" % (
                    srcdir, destdir))
                os.rename(srcdir, destdir)
                cfg.mainmodule = newmodule
        ps.current.append(run)
        ps.seen_module = True
    elif w == "delegate":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        hosttypefn, sr = get_wordfn(sr, cfg)
        if hosttypefn == None:
            ps.parseerr("expected a host type after `delegate'")
        if ps.delegated:
            ps.parseerr("a delegation session is already open")

        def run():
            # Start the delegation.
            execute.delegate_open(hosttypefn(), cfg)

            # Send a tarball of our build work directory.
            with misc.popen_rb(misc.shellquote(["tar", "-C", cfg.workpath,
                                                "-czf", "-", "."])) as tarpipe:
                data = tarpipe.read()
            execute.delegate_write(
                b"T" +
                struct.pack(">L", len(data)) + data)
        ps.current.append(run)

        ps.delegated = True
        ps.stack.append(("delegate", ps.current))
    elif w == "return":
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        if not ps.delegated:
            ps.parseerr("no delegation session open")

        # Copy file(s) back from the delegate host. We send our
        # command character "R", then a glob pattern; we then
        # repeatedly read a filename and file contents until we
        # receive zero filename length.
        patternfn, sr = get_wordfn(sr, cfg)
        if patternfn == None:
            ps.parseerr("expected a file name after `return'")
        def run():
            pattern = patternfn().encode()
            execute.delegate_write(
                b"R" +
                struct.pack(">L", len(pattern)) + pattern)

            nfiles = 0
            while 1:
                fnamelen = struct.unpack(">L", execute.delegate_read(4))[0]
                if fnamelen == 0:
                    break
                fname = execute.delegate_read(fnamelen).decode()

                datalen = struct.unpack(">L", execute.delegate_read(4))[0]
                data = execute.delegate_read(datalen)

                log.logmsg("Returned file `%s' from delegate server" % fname)
                # Vet the filename for obvious gotchas.
                if ".." in fname.split("/") or fname[:1] == "/":
                    raise misc.builderr(
                        "returned file `%s' failed security check" % fname)

                dstfile = os.path.join(cfg.workpath, fname)
                dstdir = os.path.dirname(dstfile)
                if not os.path.exists(dstdir):
                    os.makedirs(dstdir)
                with open(dstfile, "wb") as outfp:
                    outfp.write(data)
                nfiles += 1

            if nfiles == 0:
                raise misc.builderr("return statement did not match any files")
        ps.current.append(run)
    elif w == "enddelegate":
        context = ps.stack.pop()
        if context[0] != "delegate":
            ps.parseerr("`enddelegate' without matching `delegate'")
        if ps.is_config:
            ps.parseerr("`%s' command invalid in config file" % w)
        if not ps.seen_module:
            ps.parseerr("`%s' command seen before `module' command" % w)
        if not ps.delegated:
            ps.parseerr("no delegation session open")

        def run():
            execute.delegate_close()
        ps.current.append(run)

        ps.delegated = False
    elif w == "error" or w == "message":
        # Log a message, and optionally terminate the build.
        msgfn = lex_all(trim(sr), cfg)
        def run():
            msg = msgfn()
            if w == "message":
                log.logmsg("User diagnostic message: " + msg)
            else:
                raise misc.builderr("User build error: " + msg)
        ps.current.append(run)
    else:
        ps.parseerr("unrecognised statement keyword `%s'" % w)

def parse_script(scriptfile, is_config, cfg):
    ps = ParseState(is_config)

    if not is_config:
        def logfn():
            log.logmsg("Beginning execution of script file %s" % scriptfile)
        ps.program.append(logfn)

    with open(scriptfile, "r") as scriptfp:
        ps.scriptfile = scriptfile
        ps.lineno = 0
        while 1:
            ps.lineno += 1
            ps.cmdno = 1
            line = scriptfp.readline()
            if line == "": break
            parse_script_line(ps, line, cfg)

    if ps.stack[-1][0] != 'top':
        ps.parseerr("unclosed `%s' at end of script" % ps.stack[-1][0])

    if not is_config:
        def logfn():
            log.logmsg("Finished execution of script file %s" % scriptfile)
        ps.program.append(logfn)

    return ps.program

# Logging module for my build scripts.

import sys

logfp = None
logcfg = None

def init(fp, cfg):
    global logfp
    global logcfg
    logfp = fp
    logcfg = cfg

def deinit():
    init(None, None)

def to_bytes(s):
    if type(s) != bytes:
        s = s.encode()
    return s

def internal_log(msg):
    if logfp is None:
        return
    logfp.write(msg + b"\n")
    logfp.flush()
    if logcfg.verbose:
        sys.stdout.buffer.write(msg + b"\n")
        sys.stdout.buffer.flush()

def logmsg(s):
    internal_log(b"* " + to_bytes(s))

def logscript(filename, line, cmd, s):
    if cmd > 1:
        prefix = "[%s:%d #%d] " % (filename, line, cmd)
    else:
        prefix = "[%s:%d] " % (filename, line)
    internal_log(to_bytes(prefix) + to_bytes(s))

def logoutput(s):
    internal_log(b"| " + to_bytes(s))
